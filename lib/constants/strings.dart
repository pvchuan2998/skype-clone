const String MESSAGE_COLLECTION = "messages";
const String TIMESTAMP_FIELD = "timestamp";
const String USERS_COLLECTION = "user";
const String EMAIL_FIELD = "email";
const String MESSAGE_TYPE_IMAGE = "image";
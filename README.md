# Skype Clone Using Flutter and Firebase

A new Flutter application.

## Key features
1. Login/logout, google signin
2. Chat
3. Search
4. Call

## Screenshots
<img src="/uploads/d1cd3f2e70b38da23f583e635a2ee2fd/Screenshot_20210326-212259.png" width="400" height="600">
<img src="/uploads/ee06ae26ab1e59008aaf3027fe679314/Screenshot_20210326-212322.png" width="400" height="600">
<img src="/uploads/07b4fa58167c2ec9a685b0ab7a4f5730/Screenshot_20210326-212341.png" width="400" height="600">

